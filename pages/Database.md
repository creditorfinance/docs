- ## Bank account
- | Name | Type | Example | 
  |:--:|:-:|:--:|
  |Line of credit |Float|13,000|
  |Account Name| String| Citibanamex Simplicity|
  |Fecha de corte| Integer | 13 |
  |Fecha limite de pago | integer| 27|
- ## Transaction
- | Name | Type | Example | 
  |:--:|:-:|:--:|
  |Description|String|Silla corsair|
  |Amount| Float | 5,000.00|
  |Monthly payments| Bool | True|
  |Months | integer | 6 |
  |Monthly Amount| integer | 700 |
  | Date created| Date| Nov 22 |
  |Bank account | string | Amex|
  |Used by | string | otrx |
  | Comments | string | IDK |
  | Recurrent | bool |  True |
  | Cront-time | string? | 0,0,0,0|
- ## USER
- TBD